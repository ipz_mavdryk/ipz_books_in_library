﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Data.OleDb;


namespace test_s
{
    class Program
    {


        static void Main(string[] args)
        {

            IPHostEntry ipHost = Dns.GetHostEntry("localhost");

            //IPAddress ipAddr = ipHost.AddressList[0];
            IPAddress ipAddr = IPAddress.Parse("127.0.0.1");
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            string reply;
         
            string dir_name = "DB1.accdb";
            OleDbConnection connP = new OleDbConnection(); // Создаём объект соединения
            connP.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;User ID=Admin;Data Source=" + dir_name; // Строка коннекта к датабазе


            // Создаем сокет Tcp/Ip
            Socket sListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            // Назначаем сокет локальной конечной точке и слушаем входящие сокеты
            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                // Начинаем слушать соединения
                while (true)
                {
                    Console.WriteLine("Очiкування {0}", ipEndPoint);

                    // Программа приостанавливается, ожидая входящее соединение
                    Socket handler = sListener.Accept();
                    string data = null;

                    // Мы дождались клиента, пытающегося с нами соединиться

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);
                    Console.Write("текст: " + data + "\n\n");

                    if (data != "")
                    {
                        connP.Open();
                        OleDbCommand commP1 = connP.CreateCommand();
                        commP1.CommandText = data;  
                        commP1.ExecuteNonQuery(); //выполняем действие

                        if (data.Contains("SELECT") && data.Contains("SELECT DISTINCT") == false)
                        {
                            
                            OleDbDataReader dr = commP1.ExecuteReader();

                            if (dr.Read())
                            {
                                reply = "0";

                            }
                            else
                            {
                                reply = "1";

                            }

                            byte[] msg = Encoding.UTF8.GetBytes(reply);
                            handler.Send(msg);

                        }

                        int N = 0;
                        string[] b;
                        string b1="";
                        b = new string[1000];
                        byte[] msg1;
                    if (data.Contains("SELECT DISTINCT"))
                    {
                        OleDbDataReader dr = commP1.ExecuteReader();

                        while (dr.Read())
                        {
                            b[N]=dr[0].ToString();
                            b1 += b[N];
                            b1 += ";";
                            N++;
                        }

                        
                       msg1 = Encoding.UTF8.GetBytes(b1);
                        handler.Send(msg1);

                        dr.Close();
                    }

                    connP.Close();
                    }

                   

                    

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
            
           
        }
    }
}
